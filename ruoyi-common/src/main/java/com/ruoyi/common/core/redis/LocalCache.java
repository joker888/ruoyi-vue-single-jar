package com.ruoyi.common.core.redis;

import com.ruoyi.common.core.domain.CacheEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 本地缓存 代替redis 实现简单数据记录
 */
@Component
public class LocalCache {
    private static final Logger log = LoggerFactory.getLogger(LocalCache.class);
    /**
     * 默认的缓存容量
     */
    private static final int DEFAULT_CAPACITY = 512;
    /**
     * 最大容量
     */
    private static final int MAX_CAPACITY = 100000;
    /**
     * 刷新缓存的频率
     */
    private static final int MONITOR_DURATION = 2;

    /**
     * 使用默认容量创建一个Map
     */
    private static final Map<String, CacheEntity> cache = new ConcurrentHashMap<>(DEFAULT_CAPACITY);

    // 启动监控线程
    static {
        new Thread(new TimeoutTimerThread()).start();
    }

    public Collection<String> startsWithKeys(String s) {
        if ("*".equals(s)) {
            return cache.keySet();
        }else {
            return cache.keySet().stream().filter(key -> key.startsWith(s.substring(0,s.lastIndexOf("*")))).collect(Collectors.toSet());
        }
    }

//    public static boolean matches(final String key, final String pattern) {
//        StringBuffer regexp = new StringBuffer("^");
//        int i1 = pattern.indexOf("[");
//        int i2 = pattern.lastIndexOf("]");
//        boolean flag = false;
//
//        if (i1 >= 0 && i2 >= 0 && (i2 > i1)) {
//            flag = true;
//        }
//        char[] chars = pattern.toCharArray();
//        for (int i = 0; i < chars.length; i++) {
//            char c = chars[i];
//            if ('*' == c) {
//                regexp.append("\\w*");
//            } else if ('?' == c) {
//                regexp.append("\\w");
//            } else if ('[' == c) {
//                if (flag) {
//                    String substring = pattern.substring(i1, i2);
//                    String join = StringUtils.join(substring, "|");
//                    regexp.append("[").append(join).append("]");
//                    i = i2;
//                } else {
//                    regexp.append(c);
//                }
//            }
//        }
//        regexp.append("$");
//        System.out.println(regexp.toString());
//        return key.matches(regexp.toString());
//    }
//
    // 内部类方式实现单例
    private static class LocalCacheInstance {
        private static final LocalCache INSTANCE = new LocalCache();
    }

    public static LocalCache getInstance() {
        return LocalCacheInstance.INSTANCE;
    }

    private LocalCache() {
    }


    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key   缓存的键值
     * @param value 缓存的值
     */
    public <T> void setCacheObject(final String key, final T value) {
        putCloneValue(key, value, 0);
    }


    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key      缓存的键值
     * @param value    缓存的值
     * @param timeout  时间
     * @param timeUnit 时间颗粒度
     */
    public <T> void setCacheObject(final String key, final T value, final long timeout, final TimeUnit timeUnit) {
        putCloneValue(key, value, timeUnit.toSeconds(timeout));
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    public <T> T getCacheObject(final String key) {
        if (CollectionUtils.isEmpty(cache)) {
            return null;
        }
        CacheEntity<T> cacheEntity = cache.get(key);
        return cacheEntity.getValue();
    }

    /**
     * 删除单个对象
     *
     * @param key
     */
    public boolean deleteObject(final String key) {
        try {
            CacheEntity remove = cache.remove(key);
        } catch (Exception e) {
            log.error("缓存移除异常", e);
            return false;
        }
        return true;
    }

    /**
     * 删除集合对象
     *
     * @param collection 多个对象
     * @return
     */
    public long deleteObject(final Collection<String> collection) {
        long i = 0;
        for (String key : collection) {
            CacheEntity remove = cache.remove(key);
            i++;
        }
        return i;
    }

    /**
     * 将值通过序列化clone 处理后保存到缓存中，可以解决值引用的问题
     *
     * @param key
     * @param value
     * @param expireTime
     * @return
     */
    private <T> void putCloneValue(String key, T value, long expireTime) {
        if (cache.size() >= MAX_CAPACITY) {
            throw new RuntimeException("本地缓存超过最大容量");
        }
        // 序列化赋值
        CacheEntity entityClone = clone(new CacheEntity(value, System.nanoTime(), expireTime));
        cache.put(key, entityClone);
    }

    /**
     * 序列化 克隆处理
     *
     * @param object
     * @return
     */
    private <E extends Serializable> E clone(E object) {
        E cloneObject = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            oos.close();
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            cloneObject = (E) ois.readObject();
            ois.close();
        } catch (Exception e) {
            log.error("缓存序列化失败：{}", e);
        }
        return cloneObject;
    }

    /**
     * 清空所有
     */
    public void clear() {
        cache.clear();
    }


    /**
     * 过期处理线程
     */
    static class TimeoutTimerThread implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(MONITOR_DURATION);
                    checkTime();
                } catch (Exception e) {
                    log.error("过期缓存清理失败：{}", e.getMessage());
                }
            }
        }

        /**
         * 过期缓存的具体处理方法  *  * @throws Exception
         */
        private void checkTime() throws Exception {
            // 开始处理过期
            for (String key : cache.keySet()) {
                CacheEntity tce = cache.get(key);
                long timoutTime = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - tce.getGmtModify());
                // 过期时间 : timoutTime
                if (tce.getExpire() > timoutTime || tce.getExpire() == 0) {
                    continue;
                }
                log.info(" 清除过期缓存 ： " + key);
                //清除过期缓存和删除对应的缓存队列
                cache.remove(key);
            }
        }
    }
}


