package com.ruoyi.common.core.domain;

import java.io.Serializable;

public class CacheEntity<T> implements Serializable {
    private static final long serialVersionUID = -107853226360392750L;
    /**
     * 值
     */
    private T value;
    /**
     * 保存的时间戳
     */
    private long gmtModify;
    /**
     * 过期时间
     */
    private long expire;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public long getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(long gmtModify) {
        this.gmtModify = gmtModify;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }

    public CacheEntity(T value, long gmtModify, long expire) {
        super();
        this.value = value;
        this.gmtModify = gmtModify;
        this.expire = expire;
    }
}