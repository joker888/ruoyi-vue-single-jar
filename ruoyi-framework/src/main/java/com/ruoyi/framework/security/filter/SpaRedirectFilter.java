package com.ruoyi.framework.security.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

@Component
public class SpaRedirectFilter extends OncePerRequestFilter {

    public static final String API_PREFIX = "/prod-api";

    private final Logger LOGGER = LoggerFactory.getLogger(SpaRedirectFilter.class);

    // Forwards all routes except '/index.html', '/200.html', '/favicon.ico', '/sw.js' '/api/', '/api/**'
    private final String REGEX = "(?!/actuator|"+API_PREFIX+"|/_nuxt|/static|/index\\.html|/200\\.html|/favicon\\.ico|/sw\\.js).*$";
    private Pattern pattern = Pattern.compile(REGEX);

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {
        if (pattern.matcher(req.getRequestURI()).matches() && !req.getRequestURI().equals("/")) {
            // Delegate/Forward to `/` if `pattern` matches and it is not `/`
            // Required because of 'mode: history'usage in frontend routing, see README for further details
            LOGGER.info("URL {} entered directly into the Browser, redirecting...", req.getRequestURI());
            RequestDispatcher rd = req.getRequestDispatcher("/");
            rd.forward(req, res);
        } else {
            chain.doFilter(req, res);
        }
    }
}
